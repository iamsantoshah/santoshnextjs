import '../../node_modules/react-vis/dist/style.css';
import {XYPlot, LineSeries, VerticalBarSeries, MarkSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis} from 'react-vis';

const Try = () => {
  const happy = [
    {x: 0, y: 8},
    {x: 1, y: 5},
    {x: 2, y: 4},
    {x: 3, y: 9},
    {x: 4, y: 1},
    {x: 5, y: 7},
    {x: 6, y: 6},
    {x: 7, y: 3},
    {x: 8, y: 2},
    {x: 9, y: 0}
  ];
  const happy2 = [
    {x: 0, y: 9},
    {x: 1, y: 0},
    {x: 2, y: 6},
    {x: 3, y: 5},
    {x: 4, y: 9},
    {x: 5, y: 1},
    {x: 5, y: 7},
    {x: 7, y: 6},
    {x: 8, y: 3},
    {x: 9, y: 2}
  ];
  const happy3 = [
    {x: 0, y: 6},
    {x: 1, y: 3},
    {x: 2, y: 2},
    {x: 3, y: 8},
    {x: 4, y: 5},
    {x: 5, y: 4},
    {x: 6, y: 9},
    {x: 7, y: 1},
    {x: 8, y: 7},
    {x: 9, y: 0}
  ];
  return (
      <div>
        <XYPlot height={800} width={800}  colorRange={['#c7e9c0', '#00441b']}>
          <XAxis />
          <YAxis />
          <VerticalGridLines />
          <HorizontalGridLines />
          <LineSeries data={happy} />
          <LineSeries data={happy2} />
          <LineSeries data={happy3} />
        </XYPlot>
        <XYPlot height={200} width={200} colorRange={['#c7e9c0', '#00441b']} stroke='white'>
          <VerticalBarSeries data={happy} />
          <VerticalBarSeries data={happy2} />
          <VerticalBarSeries data={happy3} />
        </XYPlot>
        <XYPlot height={200} width={200} colorRange={['#c7e9c0', '#00441b']}>
          <LineSeries data={happy} />
          <LineSeries data={happy2} />
          <LineSeries data={happy3} />
        </XYPlot>
        <XYPlot height={200} width={200} colorRange={['#c7e9c0', '#00441b']} stroke='white'>
          <MarkSeries data={happy} />
          <MarkSeries data={happy2} />
          <MarkSeries data={happy3} />
        </XYPlot>
        <XYPlot
          width={300}
          height={300}>
          <HorizontalGridLines />
          <LineSeries
            color="orange"
            data={[
              {x: 1, y: 10},
              {x: 2, y: 5},
              {x: 3, y: 15}
            ]}/>
          <XAxis title="X" />
          <YAxis />
        </XYPlot>
        <XYPlot
          stackBy="y"
          width={300}
          height={300}>
          <XAxis />
          <YAxis />
          <VerticalGridLines />
          <HorizontalGridLines />           
          <LineSeries
            data={[
              {x: 1, y: 10},
              {x: 2, y: 5},
              {x: 3, y: 15}
            ]}/>
          <LineSeries
            data={[
              {x: 1, y: 12},
              {x: 2, y: 21},
              {x: 3, y: 2}
            ]}/>
        </XYPlot>
      </div>
    );
}

export default Try;
