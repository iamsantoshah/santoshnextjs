import { useState } from 'react'
import '../../node_modules/react-vis/dist/style.css';
import {XYPlot, LineSeries, VerticalGridLines, Crosshair, HorizontalGridLines, MarkSeries, LabelSeries, XAxis, YAxis} from 'react-vis';
import Select from 'react-select'
import makeAnimated from 'react-select/animated'

const Home = ({datas}) => {

    const [crosshair, setCrosshair] = useState([])
    const [multiRegions, setMultiRegions] = useState([{value: 145, label: 'India'}])
    const [markSeriesData, setMarkSeriesData] = useState([{x: datas[145].length-4, y: datas[145][datas[145].length-1], label: 'India'}])

    var startingDate = new Date("January 22, 2020");
    const customColor =  ['#1abc9c', '#f1c40f', '#2ecc71', '#e67e22', '#3498db', '#9b59b6', '#34495e',
                            '#16a085', '#f39c12', '#27ae60', '#d35400', '#2980b9', '#8e44ad', '#2c3e50']
    
    // cutomColor2 = ['#55efc4', '#81ecec', '#a29bfe', '#ffff00', '#ffeaa7', '#fab1a0', '#ff7675', '#fd79a8',
    //                         '#00b894', '#00cec9', '#0984e3', '#6c5ce7', '#fdcb6e', '#e17055', '#d63031', '#e84393']

    const regions = []
    for(let i=2; i<datas.length; i++) {
        if (datas[i][0] !== '') {
            regions.push({ value: i, label: datas[i][0] })
        }
        else {
            regions.push({ value: i, label: datas[i][1] })
        }
    }
     
    const selectedRegionsData = []
    const _confirmedCases= v => {
        const confirmedCases = []
        for(let i=4; i<datas[v].length; i++) {
            confirmedCases.push({x: i-4, y: datas[v][i]})
        }
        selectedRegionsData.push(confirmedCases)
        return confirmedCases
    }

    const _onMouseLeave = () => {
        setCrosshair([]);
    };

    const _onNearestX = (value, {index}) => {
        setCrosshair(selectedRegionsData.map(d => d[index]))
    }

    const _dayDate = e => {
        var dmy = new Date(startingDate.setDate(startingDate.getDate() + e));
        var yyyy = dmy.getFullYear();
        var dd = dmy.getDate();
        var mm = dmy.getMonth()+1;
        return (dd+'/'+mm+'/'+yyyy)
    }

    return (
        <div>

            <h2>Graphical representation of confirmed cases of COVID-19</h2>
            <p>Starting 22nd Jan 2020 as day 1 on x-axis and total number of confirmed cases on y-axis</p>
            <label htmlFor="regions">Select regions: </label>
            <div style={{ width: '1160px'}} >
                <Select
                    components={makeAnimated()}
                    options={regions}
                    placeholder='Select a region'
                    onChange={setMultiRegions}
                    isSearchable
                    isMulti
                    autoFocus
                />
            </div>
            <XYPlot
                height={800} width={1200}
                dontCheckIfEmpty
                xDomain={[0, datas.length+20]} yDomain={[0, datas[245][datas[245].length-1]]}
                onMouseLeave = {_onMouseLeave}
                margin={{left:80, right:80}}
            >
                <XAxis />
                <YAxis />
                <VerticalGridLines />
                <HorizontalGridLines />
                {multiRegions.map(({value, label}, index) => 
                    <LineSeries
                        key = {value}
                        data={_confirmedCases(value)}
                        onNearestX = {_onNearestX}
                        color={customColor[index]}
                    />
                )}
            <Crosshair
                values={crosshair}
                titleFormat={(d) => ({title: 'Day ' + (d[0].x + 1), value: _dayDate(d[0].x)})}
                itemsFormat={(d) => (multiRegions.map((e) => ({title: e.label, value: d[multiRegions.indexOf(e)].y})))}
            />
            {multiRegions.map(({value, label}, index) =>
            <MarkSeries
                key={value}
                strokeWidth={2}
                data={[{x: (datas[value].length)-4, y: datas[value][(datas[value].length)-1], label: label}]}
                color={customColor[index]}
            />
            )}
            {multiRegions.map(({value, label}) =>
            <LabelSeries
                key={value}
                data={[{x: (datas[value].length)-4, y: datas[value][(datas[value].length)-1], label: label, xOffset: 12}]}
                style={{paddingLeft: '100%', fontSize: '18px', marginLeft: '50' }}
                labelAnchorX='start'
                labelAnchorY='central'
                xOffset= '10'
            />
            )}
            </XYPlot>
        </div>
    )
}

export async function getServerSideProps() {
    const req = await fetch('https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv')
    const res = await req.text()
    const dataPre = res.split('\n')

    const datas = []
    for(let i=0; i<dataPre.length; i++) {
        datas[i+1] = dataPre[i].split(',')
    }

    return{
        props: {
            datas,
        },
    }
}

export default Home;
